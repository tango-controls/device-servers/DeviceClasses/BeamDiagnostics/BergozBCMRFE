#!/usr/bin/env python

from setuptools import setup


def main():

    setup(
        name='bergozbcmrfe',
        version='2.0.0',
        package_dir={'bergozbcmrfe': 'bergozbcmrfe'},
        packages=['bergozbcmrfe'],
        include_package_data=True,  # include files in MANIFEST
        author='Jairo Moldes',
        author_email='jmoldes@cells.es',
        description='Tango device server for the BergozBCM-RF-E instrument',
        license='GPLv3+',
        platforms=['src', 'noarch'],
        url='https://github.com/ALBA-Synchrotron/BergozBCMRFE',
        entry_points={
            'console_scripts': [
                'BergozBCMRFE = bergozbcmrfe.BergozBCMRFE:main',
            ],
        },
    )


if __name__ == "__main__":
    main()
